// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Shoot.h"

#include "AIController.h"
#include "MainPlayer.h"

UBTTask_Shoot::UBTTask_Shoot() {
	NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) {
	Super::ExecuteTask(OwnerComp, NodeMemory);

	if (!OwnerComp.GetAIOwner() || !OwnerComp.GetAIOwner()->GetPawn()) {
		return EBTNodeResult::Failed;
	}
	AMainPlayer* character = Cast<AMainPlayer>(OwnerComp.GetAIOwner()->GetPawn());
	if (!character) {
		return EBTNodeResult::Failed;
	}
	character->PullTrigger();
	return EBTNodeResult::Succeeded;
}
