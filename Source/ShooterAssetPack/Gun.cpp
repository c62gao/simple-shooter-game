// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"

#include "MainPlayer.h"
#include "DynamicMesh/DynamicMesh3.h"
#include "Engine/DamageEvents.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AGun::AGun() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(root);
	mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(root);
}

void AGun::PullTrigger() {
	UGameplayStatics::SpawnEmitterAttached(muzzleFlash, mesh,
	                                       TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(muzzleSound, mesh,
	                                     TEXT("MuzzleFlashSocket"));
	FVector ShotDirection;
	FHitResult hit;
	bool success = GunTrace(hit, ShotDirection);

	if (success) {
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), impactSound,
		                                       hit.Location);
		ACharacter* HitCharacter = Cast<ACharacter>(hit.GetActor());
		if (HitCharacter) {
			if (impactEffectCharacter) {
				UGameplayStatics::SpawnEmitterAtLocation(
					GetWorld(), impactEffectCharacter,
					hit.Location,
					hit.ImpactNormal.Rotation());
			}
			AController* OwnerController = GetOwnerController();
			if (!OwnerController) return;
			FPointDamageEvent DamageEvent(Damage, hit, ShotDirection, nullptr);
			HitCharacter->TakeDamage(Damage, DamageEvent, OwnerController,
			                         this);
		} else {
			if (impactEffectEnviroment) {
				UGameplayStatics::SpawnEmitterAtLocation(
					GetWorld(), impactEffectEnviroment,
					hit.Location,
					hit.ImpactNormal.Rotation());
			}
		}


		//DrawDebugPoint(GetWorld(), hit.Location, 20, FColor::Red, true);
	}
}

// Called when the game starts or when spawned
void AGun::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AGun::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

bool AGun::GunTrace(FHitResult& hit, FVector& shotDirection) {
	AController* OwnerController = GetOwnerController();
	if (!OwnerController) return false;
	FVector location;
	FRotator rotation;

	OwnerController->GetPlayerViewPoint(location, rotation);

	FVector End = location + rotation.Vector() * MaxRange;
	FCollisionQueryParams params;
	params.AddIgnoredActor(GetOwner());
	params.AddIgnoredActor(this);

	bool success = GetWorld()->LineTraceSingleByChannel(hit, location, End,
	                                                    ECollisionChannel::ECC_GameTraceChannel1, params);
	shotDirection =
		-rotation.Vector();
	return success;
}

AController* AGun::GetOwnerController() const {
	APawn* owner = Cast<APawn>(GetOwner());
	if (!owner) return nullptr;
	return owner->GetController();
}
