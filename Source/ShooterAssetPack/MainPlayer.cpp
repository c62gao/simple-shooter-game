// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayer.h"

#include "Gun.h"
#include "SimpleShooterGameModeBase.h"
#include "Components/CapsuleComponent.h"
#include "Engine/DamageEvents.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AMainPlayer::AMainPlayer() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMainPlayer::BeginPlay() {
	//OnTakeAnyDamage.AddDynamic(this, &AMainPlayer::TakeAnyDamage);
	Super::BeginPlay();
	Gun = GetWorld()->SpawnActor<AGun>(GunClass);
	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
	Gun->AttachToComponent(GetMesh(),
	                       FAttachmentTransformRules::KeepRelativeTransform,
	                       TEXT("weapon_socket"));
	Gun->SetOwner(this);
	Health = MaxHealth;
}

void AMainPlayer::Died() {
	isDead = true;
	ASimpleShooterGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooterGameModeBase>();
	if (GameMode) {
		GameMode->PawnKilled(this);
	}
	DetachFromControllerPendingDestroy();
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (this == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)) {
		UE_LOG(LogTemp, Warning, TEXT("You have died"));
	} else {
		UE_LOG(LogTemp, Warning, TEXT("AI has died"));
	}

}

bool AMainPlayer::IsDead() const {
	return isDead;
}

bool AMainPlayer::IsDeadForward() const {
	return deadForward;
}

float AMainPlayer::GetHealthPercent() const {
	return Health / MaxHealth;
}

// Called every frame
void AMainPlayer::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMainPlayer::SetupPlayerInputComponent(
	UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMainPlayer::PullTrigger() {
	if (Gun) {
		Gun->PullTrigger();
	}
}

float AMainPlayer::TakeDamage(float DamageAmount,
                              FDamageEvent const& DamageEvent,
                              AController* EventInstigator,
                              AActor* DamageCauser) {
	float DamageApplied = Super::TakeDamage(DamageAmount, DamageEvent,
	                                        EventInstigator,
	                                        DamageCauser);
	if (DamageApplied > 0) {
		Health = FMath::Clamp(Health - DamageApplied, 0.0f, MaxHealth);
		if (Health <= 0) {
			if (DamageEvent.IsOfType(FPointDamageEvent::ClassID)) {
				const FPointDamageEvent* PointDamageEvent =
					static_cast<const FPointDamageEvent*>(&DamageEvent);
				FVector direction = PointDamageEvent->ShotDirection;
				direction.Normalize();
				FVector forward = GetActorForwardVector();
				forward.Normalize();
				float dot = FVector::DotProduct(direction, forward);
				float angle = FMath::Acos(dot);
				float degree = FMath::RadiansToDegrees(angle);
				UE_LOG(LogTemp, Warning, TEXT("Angle: %f"), degree);
				deadForward = degree < 90;
			}
			Died();
		}
	}
	return DamageApplied;
}

// One way of causing damage
// void AMainPlayer::TakeAnyDamage(AActor* DamagedActor, float Damage,
//                                 const UDamageType* DamageType,
//                                 AController* InstigatedBy,
//                                 AActor* DamageCauser) {
// 	UE_LOG(LogTemp, Warning, TEXT("Damage From: %s, Amount: %f"),
// 	       *InstigatedBy->GetName(), Damage);
// }
