// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainPlayer.generated.h"

class AGun;

UCLASS()
class SHOOTERASSETPACK_API AMainPlayer : public ACharacter {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainPlayer();

protected:
	UFUNCTION()

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Died();

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float rotateSpeed = 70.0f;

	UFUNCTION(BlueprintPure)
	bool IsDead() const;
	UFUNCTION(BlueprintPure)
	bool IsDeadForward() const;

	UFUNCTION(BlueprintPure)
	float GetHealthPercent() const;

	bool Dead() {
		return isDead;
	}

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(
		class UInputComponent* PlayerInputComponent) override;
	UFUNCTION(BlueprintCallable)
	void PullTrigger();
	// UFUNCTION()
	// void TakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	// 					   AController* InstigatedBy, AActor* DamageCauser);
	virtual float TakeDamage(float DamageAmount,
	                         FDamageEvent const& DamageEvent,
	                         AController* EventInstigator,
	                         AActor* DamageCauser) override;

private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGun> GunClass;

	UPROPERTY(EditDefaultsOnly)
	float MaxHealth = 100;

	UPROPERTY(VisibleAnywhere)
	float Health;

	UPROPERTY()
	AGun* Gun;

	bool isDead = false;
	bool deadForward = false;
};
