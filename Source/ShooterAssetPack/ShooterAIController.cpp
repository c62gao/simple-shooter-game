// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"

#include "MainPlayer.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

void AShooterAIController::BeginPlay() {
	Super::BeginPlay();
	playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (AIBehavior) {
		RunBehaviorTree(AIBehavior);
		GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"),
		                                           playerPawn->GetActorLocation());
		GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"),
		                                           GetPawn()->GetActorLocation());
	}
}

void AShooterAIController::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
}

bool AShooterAIController::IsDead() {
	if (AMainPlayer* ControlledPawn = Cast<AMainPlayer>(GetPawn())) {
		return ControlledPawn->Dead();
	}
	return false;
}
